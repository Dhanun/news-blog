
import './App.css';
import Table from './components/table/table';
import { NewsContextProvider } from "./newsContext";
import SearchBar from './components/searchBar/searchBar'
function App() {
  return (
    <div className="App">

      <NewsContextProvider >
      <SearchBar />
      <Table />
    </NewsContextProvider>
    </div>
  );
}

export default App;
