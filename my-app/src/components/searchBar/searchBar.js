import React, { useContext, useState } from "react";
import './searchBar.css';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import { useDispatch } from "react-redux";
import { connect } from 'react-redux';
import { NewsContext } from "../../newsContext";
import { setBlogData } from "../../feature/userDataReducer";

function SearchBar(props) {
    const [searchText, getSearchValue] = useState('');
    const dispatch = useDispatch();
    const onChangeSeachText = (e) => {
        getSearchValue(e.target.value);
    }
    const handleSearchBar = (e) => {
        e.preventDefault();
        if (searchText !== "") {
            const newContactList = props && props.blogData.filter((data) => {
                return Object.values(data)
                    .join(" ")
                    .toLowerCase()
                    .includes(searchText.toLowerCase());
            });
            dispatch(setBlogData(newContactList));
        } else {
            dispatch(setBlogData(props.initialBlogData));
        }
    }
    console.log(props)
 
    return (
        <div className="searchWrapper">
            <Paper component="form" className="paperWrapper">
                <IconButton aria-label="menu">
                </IconButton>
                <InputBase
                    placeholder="Search Bar"
                    value={searchText}
                    onChange={onChangeSeachText}
                    edge='end'
                    component='div'
                />
                <IconButton type="submit" >
                    <SearchIcon onClick={handleSearchBar} />
                </IconButton>

            </Paper>
        </div>
    );
}

function mapStateToProps(state) {
    return {
        blogData: state.user.blogData,
        initialBlogData: state.user.initialInputs
    };
}

export default connect(mapStateToProps)(SearchBar);