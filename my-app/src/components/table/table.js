
import { DataGrid } from '@material-ui/data-grid';
import { Link, Avatar } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { connect } from 'react-redux';

const columns = [
    { field: 'urlToImage', headerName: 'Image', width: 200, sortable: false, renderCell: (row) => <Avatar variant='square' alt="article-thumbnail" src={row.row.urlToImage}  /> },
    { field: 'source.name', headerName: 'Source', width: 200, sortable: false, renderCell: (row) => <div>{row.row.source.name}</div> },
    { field: 'author', headerName: 'Author', width: 200, sortable: false },
    {
        field: 'title',
        headerName: 'Title',
        width: 200,
        sortable: false
    },
    {
        field: 'publishedAt',
        headerName: 'Date',
        sortable: true,
        width: 160,
    },
    {
        field: 'url',
        headerName: 'Url',
        width: 500,
        renderCell: (row) => <Link href={row.row.url} target="_blank">{row.row.url}</Link>,
        sortable: false
    }
];



function DataTable(props) {

    const { blogData } = props;
    return (
        <Paper style={{ height: 600 }}>{
            blogData ? <DataGrid rows={blogData} columns={columns} pageSize={10} disableColumnMenu /> : ''
        }
        </Paper>
    );
}
function mapStateToProps(state) {
    return {
        blogData: state.user.blogData
    };
}


export default connect(mapStateToProps)(DataTable);