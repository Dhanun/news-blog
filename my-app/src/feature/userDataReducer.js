import { createSlice } from "@reduxjs/toolkit";
const userData = createSlice({
    name: "user",
    initialState: {
        initialInputs: null,
        blogData: null,
    },
    reducers: {
        setInput: (state, action) => {
            state.initialInputs = action.payload;
        },
        setBlogData: (state, action) => {
            state.blogData = action.payload;
        },
    },
});

export const {
    setInput,
    setBlogData,
} = userData.actions;
export const selectInitialBlogInput = (state) => state.user.initialInputs.articles;
export const selectBlogData = (state) => state.user.blogData.articles;
export default userData.reducer;