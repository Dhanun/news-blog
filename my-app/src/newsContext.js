import React, { createContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { setBlogData, setInput } from "./feature/userDataReducer"

export const NewsContext = createContext();

export const NewsContextProvider = (props) => {
    const [data, setData] = useState();
    const dispatch = useDispatch();
    const apiKey = "85b502496dc942228e315ec887cc2d69";

    useEffect(() => {
        axios
            .get(
                `http://newsapi.org/v2/everything?q=rich&from=2021-03-30&sortBy=publishedAt&apiKey=${apiKey}&pageSize=100`
            )
            .then((response) => {
                setData(response.data.articles);
            })
            .catch((error) => console.log(error));

    }, []);

    data && data.map((data, index) => {
        data.id = index + 1;
        index++;
        return data;
    })
    
    dispatch(setInput(data));
    dispatch(setBlogData(data));
    return (
        <NewsContext.Provider value={{ data }}>
            {props.children}
        </NewsContext.Provider>
    );
};