
import { configureStore } from "@reduxjs/toolkit";
import userReducer from "../feature/userDataReducer";

export default configureStore({
    reducer: {
        user: userReducer,
    },
});